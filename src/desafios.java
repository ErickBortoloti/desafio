import java.util.Scanner;

public class desafios {
    public static void main(String[] args) {

        System.out.println("DESAFIO 1 RESULTADO:");
        System.out.println(new desafio1().calcularSoma());
        System.out.println("------------------------");

        System.out.print("DESAFIO 2 ");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite um número para verificar se pertence à sequência de Fibonacci: ");
        int numero = scanner.nextInt();

        desafio2 checker = new desafio2();
        checker.verificarPertencimento(numero);
        System.out.println("------------------------");

        System.out.println("DESAFIO 3 RESULTADOS:");
        verificarProximoElemento("a", 7);
        verificarProximoElemento("b", 64);
        verificarProximoElemento("c", 36);
        verificarProximoElemento("d", 64);
        verificarProximoElemento("e", 8);
        verificarProximoElemento("f", 19);
        System.out.println("------------------------");


        System.out.println("Você está na sala das lâmpadas. Ligue um interruptor e, após alguns minutos, desligue-o. Em seguida, ligue outro interruptor.");
        System.out.print("Após este procedimento, informe o estado das lâmpadas (acesa, quente ao toque, fria): ");
        String estadoLampadas = scanner.next();

        desafio4.desvendarInterruptores(estadoLampadas);
        System.out.println("------------------------");

        System.out.println("Desafio 5");
        System.out.print("Digite uma string: ");
        String input = scanner.next();

        String reversed = desafio5.reverseString(input);
        System.out.println("String invertida: " + reversed);
    }







    public static void verificarProximoElemento(String sequencia, int ultimoElemento) {
        desafio3 s;
        switch (sequencia) {
            case "a":
                s = new desafio3(1, 2);
                break;
            case "b":
                s = new desafio3(2, 2);
                break;
            case "c":
                s = new desafio3(0, 1);
                break;
            case "d":
                s = new desafio3(4, 2);
                break;
            case "e":
                s = new desafio3(1, 1);
                break;
            case "f":
                s = new desafio3(2, 4);
                break;
            default:
                throw new IllegalArgumentException("Sequência inválida.");
        }

        System.out.println("Próximo elemento da sequência " + sequencia + "): " + s.proximoElemento(ultimoElemento));
    }
}
