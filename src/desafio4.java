public class desafio4 {
    public static void desvendarInterruptores(String estadosLampadas) {
        String[] estados = estadosLampadas.split(" ");

        if (estados.length < 2) {
            System.out.println("Por favor, forneça dois estados de lâmpada separados por um espaço.");
            return;
        }

        String primeiraVisita = estados[0];
        String segundaVisita = estados[1];

        String interruptor1 = "", interruptor2 = "", interruptor3 = "";

        if (primeiraVisita.equals("acesa")) {
            interruptor1 = "Controla a lâmpada acesa";
        } else if (primeiraVisita.equals("quente")) {
            interruptor2 = "Controla a lâmpada quente";
        } else {
            interruptor3 = "Controla a lâmpada fria";
        }

        if (segundaVisita.equals("acesa")) {
            interruptor1 = "Controla a lâmpada acesa";
        } else if (segundaVisita.equals("quente")) {
            interruptor2 = "Controla a lâmpada quente";
        } else {
            interruptor3 = "Controla a lâmpada fria";
        }

        System.out.println(interruptor1);
        System.out.println(interruptor2);
        System.out.println(interruptor3);
    }
}
