public class desafio3 {
    private int inicio;
    private int incremento;

    public desafio3(int inicio, int incremento) {
        this.inicio = inicio;
        this.incremento = incremento;
    }

    public int proximoElemento(int ultimoElemento) {
        return ultimoElemento + incremento;
    }

}
