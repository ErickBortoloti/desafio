public class desafio2 {
    public void verificarPertencimento(int numero) {
        int fib1 = 0;
        int fib2 = 1;
        int proximoFib = fib1 + fib2;

        while (proximoFib <= numero) {
            if (proximoFib == numero) {
                System.out.println(numero + " pertence à sequência de Fibonacci.");
                return;
            }
            fib1 = fib2;
            fib2 = proximoFib;
            proximoFib = fib1 + fib2;
        }

        System.out.println(numero + " não pertence à sequência de Fibonacci.");
    }
}
